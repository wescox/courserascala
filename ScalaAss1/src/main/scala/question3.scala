/**
  * Created by wescox on 4/2/17.
  */
object question3 extends App {
  def countChange(money: Int, coins: List[Int]): Int = {
    if (coins.isEmpty) 0 // No coins available
    else if (money == 0) 0 // No money to repay, we cannot make change
    else if (money - coins.head == 0) 1 // Makes clean change, this is a way
    else if (money - coins.head > 0)
      //          skip coin                 use coin
      countChange(money, coins.tail) + countChange(money-coins.head, coins)
    else
      //          skip coin
      countChange(money, coins.tail)

  }

  println("4 from [1,2] " + countChange(4, List(1, 2)))
  println("0 from [1,2] " + countChange(0, List(1, 2)))
  println("4 from [] " + countChange(4, List()))
  println("9 from [2] " + countChange(9, List(2)))
  println("10 from [2] " + countChange(10, List(2)))




//  def countChangeAl(money: Int, coins: List[Int]): Int = {
//    def loop(money: Int, coins: List[Int], iter: Int): Int = {
//      if (money < 0) 0
//      else if (money == 0) 1
//      else if (iter <= 0  && money > 0) 0
//      else  loop( money , coins, iter -1 ) + loop(money - coins(iter-1) , coins, iter);
//    }
//    loop(money, coins, coins.size)
//  }
//
//
//  println("Al 4 from [1,2] " + countChangeAl(4, List(1, 2)))
//  println("Al 0 from [1,2] " + countChangeAl(0, List(1, 2)))
//  println("Al 4 from [] " + countChangeAl(4, List()))
//  println("Al 9 from [2] " + countChangeAl(9, List(2)))
//  println("Al 10 from [2] " + countChangeAl(10, List(2)))
}
