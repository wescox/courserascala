/**
  * Created by wescox on 4/2/17.
  */
object question2 extends App {
  def balance(chars:List[Char]): Boolean = {
    def countParen(numOpen: Int, chars: List[Char]): Boolean = {
      if (numOpen < 0) false

      else if (chars.isEmpty) numOpen == 0

      else if (chars.head == '(') countParen(numOpen + 1, chars.tail)

      else if (chars.head == ')') countParen(numOpen - 1, chars.tail)

      else countParen(numOpen, chars.tail)
    }

    countParen(0, chars)
  }
  println("balance('(if (zero? x) max (/ 1 x))') " + balance("(if (zero? x) max (/ 1 x))".toList));
  println("balance('I told him (that it’s not (yet) done). (But he wasn’t listening) " + balance("I told him (that it’s not (yet) done). (But he wasn’t listening)".toList));
  println(":-) " + balance(":-)".toList));
  println("())( " + balance("())(".toList));
  println("balance('(just an) example') " + balance("(just an) example".toList));

  println("EMPTY " + balance("".toList));
}
