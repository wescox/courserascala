/**
  * Created by wescox on 4/2/17.
  */
object question1 extends App {
  def pascal(c: Int, r: Int): Int = {

    if (c == 0) 1
    else if (c == r) 1
    else pascal(c - 1, r - 1) + pascal(c, r-1)
  }

  println("pascal(0,2) " + pascal(0,2))
  println("pascal(1,2) " + pascal(1,2))
  println("pascal(1,3) " + pascal(1,3))
}
